# 1. Gestion de projet et documentation

Ce premier projet nous a montré comment fonctionnent **git**, les **clefs SSH**, le **mark down**, **imageMagick** et l'utilisation d'un **terminal de commande**, le tout sur linux.

J'utilise habituellement une VM ([machine virtuelle](https://fr.wikipedia.org/wiki/Machine_virtuelle)) pour simuler un environnement linux (ubutunu pour mon cas). Cependant, aucune d'entre elles ne fonctionnaient au début du projet, j'ai donc dû reconfigurer mon git et imageMagick et apprendre (un peu) le terminal de commande de **Windows**.  

## Git

**Git** est un **controleur de version**. 
C'est à dire que pour pouvoir travailler efficacement à plusieurs il faut synchroniser les versions, c'est là qu'intervient git. 
Pour chaque *projet* il y a la branche principale *"main"* et chacun copie sur une des branches puis fait des modifications en local et renvoie ses modifications sur la branche qu'il a copiée (cela peut être la branche *main*).
Comme ça, plusieurs personnes peuvent travailler sur une même partie de projet (ou sur un projet tout entier) et à chaque fin de session, les personnes qui travaillent envoient leurs modifications et tout est **sauvegardé** (comme ça s’il y a une erreur on peut revenir en arrière) et les autres téléchargent la dernière version au moment de se mettre au travail.

L'utilisation de **git** pour ce projet m'a été assez *simple* car étant en BA3 informatique, j'ai eu l'occasion de me casser la tête dessus auparavant. 
Cependant, cela m'a un peu rafraichi la mémoire pour l'utilisation des **commandes** telles que :
```
# (avec une clef SSH) Copie un dépôt en s'identifiant avec son empreinte digitale (pass phrase et clef privée/publique)
git clone <lien>

# Récupère la dernière version sur le dépôt et la met en local pour qu'on puisse travailler dessus
git pull 

# Ajoute des éléments (fichiers ou dossiers) à commit
git add <dossier ou fichier>

# Prépare l'envoi avec -m pour ajouter un commentaire 
git commit [- m <commentaire>]

# Envoye les modifications (éléments dans le commit) que l'on a effectuées par rapport à la branche
git push

# Savoir où on en est par rapport à la branche à laquelle on est raccordé
git status

```

![git](images/module_1/commandes_git.png)

## Clefs SSH

Une **clef SSH** est constituée d'une **clef privée** et d'une **clef publique**, la première est un identifiant entre nous et un programme et la deuxième un identifiant entre les autres et nous (qu'ils puissent nous identifier sans nous voler notre identité).

J'ai déjà utilisé ce genre de **sécurité** pour d'autres projets, mais jamais par ligne de commande (ça a toujours été des fonctions de libraires externes dans du code).

Ici, j'ai utilisé le protocole **ED25519** qui est plus sécurisé que le **RSA** seul et j'ai suivi le protocole donné par **gitlab**.

![protocole](images/module_1/SSH.png)

## Mark down

Le **markdown** est un langage de programmation assez similaire au **html** (bien que plus simple que ce dernier).
Les fichiers markdown sont en **".md"** (comme une image peut être en **.png** ou **.jpeg**). 
La syntaxe peut être retrouvée *[ici](https://www.markdownguide.org/)*. 
Cependant, voilà certains points importants :
```
# mettre un texte en gras
**texte**

# mettre un text en italique
*texte*

# mettre une barre sur un texte
~~texte~~

# titre
## sous-titre
### sous-sous-titre
...

# mettre un lien dans du texte
[légende](lien)

# affichier une image
![légende](chemin d'accès)

# case à cocher ou non (avec le 'x')
[x] 

# faire une liste
- point de la liste
- 2ème point
- ...
```

NB : il est à noter qu'on peut combiner des points :
- comme mettre un lien en **~~gras~~** *italique* : *[test](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/louis.devroye/-/tree/main/docs)*

Le **mark down** a été totalement nouveau pour moi. Cependant, il s'est avéré très simple d'utilisation et a été très rapide à prendre en mains. J'ai utilisé (et j'utilise en ce moment même) [Dillinger](https://dillinger.io) pour pouvoir *prévisualiser* le rendu.

![dillinger](images/module_1/markdown.png)

## Terminal

Un **terminal de commandes** et un programme utilisé depuis toujours pour communiquer avec l'ordinateur. Le plus souvent, il *ne possède pas d'interface graphique* (donc pas de bouton, animations, gestion de la souris). 
L'utilisateur peut y entrer des commandes que l'ordinateur va exécuter (ou non si l'utilisateur n'a pas la permission).  

Le **terminal** est une chose très importante dans la vie d'un étudiant en informatique. J'ai plus l'habite d'utiliser les commandes de linux telles que *ls* *rm*, etc.

J'ai lié mon compte gitlab sur mon terminal avec les commandes données par **gitlab**.

![commandes](images/module_1/git_terminal.png)

## ImageMagick

Pour ne pas utiliser trop d'espace de stockage, un logiciel tel que **[ImageMagick](https://imagemagick.org/script/download.php)** est très utile. 
En effet, il permet de **compresser** et **d'éditer** des images en toute simplicité.

**ImageMagick** est un programme de traitement d'image comme *[Gimp](https://www.gimp.org/)* par exemple ou *[Photoshop](https://www.adobe.com/fr/products/photoshop/landpb.html?skwcid=AL!3085!10!79096303516512!79096313612708&mv=search&sdid=LZ32SYVR&ef_id=4070382b271119cfd6955bdd7772130b:G:s&s_kwcid=AL!3085!10!79096303516512!79096313612708&skwcid=AL!3085!10!79096303516512!79096313612708)*.
Il permet de compresser et d'éditer facilement des images.

Par un heureux hasard, j'ai déjà utilisé **ImageMagick** sur linux pour un projet d'OS où il fallait compresser des images à la demande dans un script bash.

Cependant, j'ai téléchargé l'application **Windows** pour plus de simplicité.

![imageMagick application](images/module_1/utilisation_imageMagick.png)

Une fois dedans, il faut sélectionner **"view"** et réduire l'image jusqu'à la taille souhaitée.

## Les principes de project managment

Pour ce projet, je me suis informé sur les [project management principles](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/project-management-principles.md). 
J'ai encore quelques soucis pour certains points.

| Point                               | mise en pratique | explication                                                                                                                                | commentaires                                                                                          |
|-------------------------------------|------------------|--------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| **As-you-work**                     | partielle        | Faire les choses petit à petit et pas juste à la fin en une grosse fois.                                                                   | j'ai du mal à me commencer un projet mais une fois lancé (peu importe la date butoire) je suis à fond |
| **Spiral development**              | totale           | Commencer par la base et développer le projet autour de ça.                                                                                | /                                                                                                     |
| **Serial development**              | partielle        | Faire plusieurs tâches en même temps pour prévoir le futur et avancer sur tout en voyant les problèmes que les autres parties impliques.   | je regarde trop la fin du projet comme le spiral development                                          |
| **Modular & Hierarchical Planning** | quasi totale     | Planifier différentes sous parties du projet et les classer en fonction de l'importance.                                                   | j'arrive à scinder mes projets mais pas à planifier                                                   |
| **Triage**                          | totale           | Prioriser les parties critiques du projet.                                                                                                 | /                                                                                                     |
| **Supply-Side Time Management**     | très partielle   | Planifier le temps que chaque tâche prends et le temps que l'on a à accorder en tout et ne pas passer plusieurs heures sur une même tâche. | je n'aime pas planifier un agenda pour mes projets, je fonctionne souvent en deadline                 |

**Pour moi**, l'utilisation de petites *deadlines* fonctionne le mieux, cela permet de ne pas se projeter de trop et de voir l'avancement du projet. 
J'utilise le **spiral development** pour me concentrer sur les bases et ne pas trainer à tout anticiper (même s'il faut anticiper un peu et garder du recul avec du **serial development**).
Faire des petites modifications comme **As-you-Work** permet de se mettre au travail et de lancer le train du travail.

# Check-list 
- [X] Make a website and describe how you did it
- [X] Introduced yourself
- [X] Documented steps for uploading files to the archive
- [X] Documented steps for compressing images and keeping storage space low
- [X] Documented how you are going to use project management principles 
- [X] Pushed to the class archive