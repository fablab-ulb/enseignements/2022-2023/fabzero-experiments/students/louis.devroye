# 2. Conception Assistée par Ordinateur (CAO)

Cette semaine, j'ai travaillé sur la conception d'un **connecteur flexible** ([FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks)) de briques **[Lego](https://fr.wikipedia.org/wiki/Lego)**. 

Je me suis basé sur les informations et indications de l'[Assignment 2 :Computer-Aided Design (CAD)](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues/2).

## Description

Un logiciel **[CAD](https://en.wikipedia.org/wiki/Computer-aided_design)** (computer aided design) est un logiciel permettant de créer des objets **2D** et **3D**. 
Ceux-ci peuvent être **partagés** et **imprimés** (par une [**imprimante 3D**](https://fr.wikipedia.org/wiki/Impression_3D)).  

Voici un exemple d'objet 3D que j'ai conceptualisé (sur base d'une brique [Lego](https://fr.wikipedia.org/wiki/Lego)) :


![copie brique lego](images/module_2/lego_brick.png)

## Liens utiles

| Nom                                                                                                                           | Description                                                                                          |
|-------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------|
| [OpenSCAD cheat sheet](https://openscad.org/cheatsheet/)                                                                      | Aide-mémoire openSCAD                                                                                | |
| [OpenSCAD (2021.01)](https://openscad.org/)                                                                                   | Logiciel de conception 2D et 3D                                                                      |
| [FreeCAD 0.20](https://www.freecad.org/)                                                                                      | Logiciel de conception 2D et 3D                                                                      |
| [Inkscape (1.2.2)](https://inkscape.org/fr/)                                                                                  | Logiciel de dessin (vectoriel)                                                                       |
| [Creative Commons (4.0)](https://creativecommons.org/about/cclicenses/)                                                       | Législation et appellations des droits d'auteurs                                                     |
| [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks)                                                                | Kit de construction et "compliant mechanisms & flexures"                                             |
| [CAD class tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad)                                                 | Tutoriel sur l'utilisation de logiciels (CAD et autres) liés au projet                               |
| [PrusaSlicer (2.5.0)](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.38260715.952840959.1677750636-206575916.1677163197) | Entreprise d'imprimante 3D, il y a un logiciel qui va avec pour construire et importer ses objets 3D |
| [Thingiverse](https://www.thingiverse.com/)                                                                                   | Plateforme de partage d'objet 3D                                                                     |

**Tout le code et les fichiers qui ont été nécessaires sont dans le dossier file/module_2**

## FlexLink et Compliant mechanisms & flexures

Les [**FlexLinks**](https://www.compliantmechanisms.byu.edu/flexlinks) sont des objets d'utilité diverse, imprimés en 3D et pouvant s'emboiter sur des pièces **Lego**. Ils font partie des "**Compliant mechanisms & flexures**". 
Il s'agit d'objets imprimés en 3D, ils **doivent** n'être constitué **que d'une pièce**. Leur **flexibilité** dans différents axes, combiné aux endroits où ils peuvent **s'accrocher** permet de faire des objets dit "**Low-tech**".

La [**Low-tech**](https://fr.wikipedia.org/wiki/Low-tech) est un **mouvement de pensée** et une méthode de création d'objet visant à résoudre des problèmes. 
Ces objets visent à en **recréer** d'autres plus courant, mais en les **simplifiant** au maximum pour en avoir le maximum du **potentiel**.
Des exemples d'objets peuvent être : des pinces, des boutons, des leviers et beaucoup d'autres. 

## Mon choix de logiciel de CAO

J'ai choisi d'utiliser **OpenSCAD** car ce dernier me semble être le plus simple de prise en main. 
Comparé à **FreeCAD** qui me paraissait trop opaque et à **InkScape** qui ne correspondait pas à mes besoins.  

**OpenSCAD** est aussi très performant et peut être avoir des fonctionnalités assez poussées comme les **hulls**, etc.
La **CheatSheet** a aussi aidé et le format *programmation* de ce logiciel m'a permis de retrouver un certain *confort*.

## OpenSCAD

[OpenSCAD (2021.01)](https://openscad.org/) est un logiciel de **conception 3D** (Computer-Aided Design, CAD). Il est **libre d'accès**. 
Contrairement à d'autres logiciels CAD, il faut utiliser des **lignes de commandes** pour créer, modifier et déplacer des objets.

### Exemple de code pour créer des figures 3D et 2D basiques.

Le plus utile pour cela est la [**Cheat sheet OpenSCAD**](https://openscad.org/cheatsheet/)

```
// Créer un cube, si "center=true" est spécifié, les coordonnées du cube sont par rapport à son centre.
cube([longueur, largeur, hauteur], center=true);

// D'autres formes basiques :

// "r=" indique le rayon et "d=" le diamètre. Il y a aussi la possibilité de centrer ces figures.
cylinder(r=radius);
sphere(r=radius, $fn=50);

// Nous pouvons noter un nouveau paramètre :
//     "$fn" qui peut être traduit par "le nombre de facettes", il peut être général au programme ou à la figure qu'on créer.

square([length, width]);
square(side) // Il s'agit de la même fonction qu'au-dessus mais avec des paramètres différents.

// Certaines méthode d'itération tel que le for existe.
for (i= [0:n]) {
  //Code répété à chaque itération
}

// Nous pouvons aussi translater un objet.
translate([x,y,z]) {
  //Objet à translater
}

// Une méthode de création d'objets existe aussi.
module Nom(param 1, param 2) {
  //Code du module
}

// Il est possible d'appeler ce module avec "Nom(param 1, param 2);".
```

Pour exporter l'objet, il faut **calculer le rendu de la pièce** (F6) et puis exporter au format **.stl** (F7) (et, plus tard, glisser le fichier **.stl** dans **PrusaSlicer**).

### Création du flexible lego connector 
  
Le code se trouve dans le fichier **./files/module_2/flexible_lego_connector.scad**
Les étapes pour recréer le **flexible lego connector** sont assez simple à expliquer. 
J'ai utilisé une méthode qui se rapprocherait plus de la sculpture selon moi (avoir une base et enlever des choses dedans, tailler dedans) :

- Créer un cube comme base avec deux cylindres en les connectant avec un hull.
  ```
  module base(length, width, height, color="yellow") {
    
    // without the hull we must use a cube inbetween the two cylinders
    // cube([length, width, height], center=true);
    color(color, 1.0) {
        r_cyl = width/2;
        hull(){
            for (i=[-1:1]) {
                if (i!=0) {
                    translate([i*length/2,0,0]) {
                        cylinder(height, r=r_cyl, center=true, $fn=50);
                    }
                }
            }
        }
    }
  }
  ```
- Faire la différence avec des cubes à bord ronds pour créer les deux parties "fragiles" du milieu en gardant une petite base solide au milieu.
  ```
  module rounded_cube(length, width, height, color="red") {
    
    //diam = length < width ? length/2 : width/2;
    color(color, 1.0) {
        minkowski(){
            cube([length, width, height], center=true);
            sphere(d=length/2, $fn=50);
        }
    }
  }
  
  module negativ_cubes(length, width, height){
    
    //middle
    translate_x = length/2;
    translate_z = height*PROPORTION_MIDDLE_CUBE;
    translate([0,0,translate_z*2.2]) {
        rounded_cube(length, width, height/2);
    }
        
    //left
    translate([translate_x,0,translate_z/2]) {
        rounded_cube(length/3, width, height,"blue");
    }
    
    //right
    translate([-translate_x,0,translate_z/2]) {
        rounded_cube(length/3, width, height,"green");
    }
  }
  ```
- Faire des trous pour laisser deux **Lego** s'emboiter dedans (un de chaque côté). 
  ```
  module cylinder_holes(depth, radius, space_between) {

  cylinder(depth, r=radius, center=true, $fn=100);


    translate([space_between,0,0]) {
        cylinder(depth, r=radius, center=true, $fn=100);
    }
  }
  ```

La difficulté réside surtout dans la valeur des paramètres pour l'impression 3D. 
```
//////// global variables 

// space between the two holes (cylinders)
SPACE_BETWEEN=8;

// diameter of holes (cylinders)
DIAMETER_HOLE=4.85;

//error between real life and openscad
ERROR=0.22;

// scale of the middle cubes depending on the height of the piece
PROPORTION_MIDDLE_CUBE=1;

// difference to get rid of the artifacts in differences
EPSILON=1;

// scale
SCALE=1;

// main variables
LENGTH=37;
WIDTH=LENGTH/4.5;
HEIGHT=1.8;
```

## Creative commons (CC) et en-tête

Les **droits d'auteurs** sont très importants et très regardés de nos jours. Grâce aux [Creative Commons (4.0)](https://creativecommons.org/about/cclicenses/), chaque créateur **peut** mettre des conditions sur ses projets et sur leur **accessibilité** et disponibilités envers le grand publique.
Et ce, grâce aux licences que **Creative Commons** offre, il en existe plusieurs types pour chaque envie du créateur.

| CC                                                                | Description                                                                           |
|-------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| [CC0](https://creativecommons.org/publicdomain/zero/1.0/)         | Le créateur laisse ses tomber ses droit dans le domaine publique.                     |
| [CC-BY](https://creativecommons.org/licenses/by/4.0/)             | L'utilisateur peut partager ou modifier (commercialement) le projet.                  |
| [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)       | Comme "BY", il faut juste partager avec les mêmes contraintes que le projet original. |
| [CC BY-NC](https://creativecommons.org/licenses/by-nc/4.0/)       | Comme "BY" sauf que l'utilisateur ne peut pas rendre son projet commercial.           |
| [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) | Combinaison de "BY-NC" et "BY-SA".                                                    |
| [CC BY-ND](https://creativecommons.org/licenses/by-nd/4.0/)       | L'utilisateur peut seulement partager (même commercialement) le projet.               |
| [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/) | L'utilisateur peut seulement partager le projet sans but commercial.                  |

Pour appliquer la licence souhaitée à votre projet, il suffit d'indiquer :
 
- Creative Commons.

- Le type de CC choisis avec la version. 

- Le lien vers cette licence. 

Le tout, de préférence dans **l'en-tête** ou dans un fichier **license.txt** prévu à cet effet.

L'exemple ci-dessous montre comment appliquer le **CC BY-SA 4.0**

```
LICENSE : Creative Commons, 
      Attribution-ShareAlike 4.0 International [CC BY-SA 4.0] 
      https://creativecommons.org/licenses/by-sa/4.0/).
```

### En-tête

Tout projet doit avoir une référence vers **son auteur**, la **date de conception**, les **versions** du projet, un **descriptif** et sa **licence**.
![Exemple en-tête](images/module_2/en-tete.PNG)

## Conclusion

Ce module sur la **conception** et l'**impression** d'objets 3D a été une bonne initiation. Il m'a permis de bien comprendre les difficultés et les solutions qu'il y a pour créer des objets. 
De la conception à l'impression, il est très difficile de penser à tous les détails et de prévoir les différentes proportions tout en gardant un code propre, compréhensible et paramétrable. 

![rendu final](images/module_2/final.png)

## Check-list

- [X] Design, model, and document a parametric FlexLinks construction kit that you will
  **fabricate next week using 3D printers.**
- [X] Make it parametric, so you will be able to make adjustments accounting for the
  **material properties and the machine characteristic**
- [X] Read about Creative Commons open-source Licence and add a CC license to your work.
- [X] (**Next Week**) Complete your FlexLinks kit that you made by fetching the code of at least 1 part made by 
  **another classmate during this week. Acknowledge its work. Modify its code and get the parts ready to print.**
