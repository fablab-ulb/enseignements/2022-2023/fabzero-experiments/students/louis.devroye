# 3. Impression 3D

Cette semaine, j'ai travaillé sur l'impression d'un objet 3D créé par mes soins (et repris de [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks)).

## Description

Après avoir créé un objet grâce à un logiciel de *design* (**CAD/CAO**). 
Il faut passer à l'**impression**. Le logiciel d'exportation 3D [**PrusaSlicer**](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.38260715.952840959.1677750636-206575916.1677163197) de l'entreprise Prusa permet d'importer un objet en **3D** avec des dimensions et des paramètres choisis par l'utilisateur. 

Pour commencer ce module, il faut un objet "**.stl**". Ceci est possible via [OpenSCAD](https://openscad.org/) en exportant l'objet sur lequel on travail.

![final](images/module_2/final.png)

## Liens utiles

| Nom                                                                                                                            | Description                                                                                          |
|--------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------|
| [OpenSCAD cheat sheet](https://openscad.org/cheatsheet/)                                                                       | Aide-mémoire openSCAD                                                                                |
| [OpenSCAD (2021.01)](https://openscad.org/)                                                                                    | Logiciel de conception 2D et 3D                                                                      |
| [FreeCAD 0.20](https://www.freecad.org/)                                                                                       | Logiciel de conception 3D et 2D                                                                      |
| [Inkscape (1.2.2)](https://inkscape.org/fr/)                                                                                   | Logiciel de dessin (vectoriel)                                                                       |
| [Creative Commons (4.0)](https://creativecommons.org/about/cclicenses/)                                                        | Législation et appellations des droits d'auteurs                                                     |
| [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks)                                                                 | Kit de construction et "compliant mechanisms & flexures"                                             |
| [CAD class tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad)                                                  | Tutoriel sur l'utilisation de logiciels (CAD et autres) liés au projet                               |
| [PrusaSlicer (2.5.0)](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.38260715.952840959.1677750636-206575916.1677163197)  | Entreprise d'imprimante 3D, il y a un logiciel qui va avec pour construire et importer ses objets 3D |
| [Thingiverse](https://www.thingiverse.com/)                                                                                    | Plateforme de partage d'objet 3D                                                                     |
| [Thibault Leonard](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault/)        | Étudiant fablab (binôme de soutiens) avec qui j'ai fait le flexlink                                  |
| [Victor De Pillecyn](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/)     | Étudiant fablab avec qui j'ai fait le flexlink                                                       |

## Imprimante 3D Prusa et logiciel


Les imprimantes **Prusa** ont une *spécificité* qui est qu'elles sont faites avec des pièces en **PLA** (le matériau utilisé pour créer des objets dans ces mêmes imprimantes). 
Il est donc **possible** de les réparer en **imprimant** la pièce cassée (si les conditions le permettent). 

Les **imprimantes 3D** se développent de plus en plus. [Prusa](https://www.prusa3d.com/fr) est une entreprise d'imprimantes 3D. 
Nous allons travailler avec celles-là et avec le logiciel de découpe 3D [**PrusaSlicer 2.5.0**](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.38260715.952840959.1677750636-206575916.1677163197) qui va avec.

Une imprimante 3D est une imprimante avec la spécificité de créer un objet 3D en faisant fondre du plastique (**PLA** souvent) et en construisant l'objet couche par couche. Cet objet a donc une structure beaucoup plus complexe qu'un simple dessin sur une feuille A4.
Ce type d'imprimante est connu pour être long (plusieurs heures pour des pièces de 5-10cm) et l'impression échoue assez **fréquemment** à cause la conversion objet 3D → monde réel (angles trop grands, adhésion de la pièce, surchauffe, etc.). 

### Mise en place

Pour **importer** un objet, il suffit de **glisser** le fichier (**.stl** dans notre cas) dans le logiciel ouvert.

Ensuite, pour **agencer sa pièce** et mettre **son milieu** au *milieu du plateau*, il faut cliquer sur **ce bouton** :
![agencer](images/module_3/agencer.PNG)

Pour positionner la face que l'on veut vers **le bas**, il faut cliquer sur **ce bouton** et puis sur **la face** que l'on veut positionner vers **le sol**.

![positionner](images/module_3/positioner.PNG)

### Paramètres 

Différents **paramètres** sont disponibles en haut à droite du logiciel *Prusa*. 
Les paramètres en dessous sont les plus basiques :

- 0.20mm est **l'épaisseur du filament** de plastique. Au plus c'est haut, au plus le résultat sera de *"qualité"*, mais l'impression prendra plus de temps. (le minimum, 0.1 signifie plus rapide mais moins résistant et 0.3, le maximum signifie plus lent mais plus résistant)

- Le **type de filament** a été imposé ici. **PLA** est un filament très basique.

- Le **pourcentage de remplissage** de la pièce est généralement entre 20 et 30 (%). 20 étant assez solide et rapide, 30 signifie très solide mais moins rapide et en dessous, moins solide mais plus rapide. 

- Les **supports** permettent de supporter la pièce aux endroits où il y a des angles et où la pièce est dans le vide. Cela prend forcément du temps d'impression mais c'est **nécessaire** pour certaines pièces.

- La **bordure** permet de délimiter la pièce pour améliorer l'adhérence (et voir s'il y a une erreur de position dans l'impression très rapidement). Cela prend un tout petit temps d'impression mais est souvent très utile. Au **moins** la pièce possède de surface en contact avec le plateau, au **plus** c'est utile. 
L’imprimante est une imprimante [Prusa](https://www.prusa3d.com/fr) comme mentionné plus haut.

![paramètres](images/module_3/params.PNG).

![prusa_1](images/module_3/prusa_1.PNG)

Un fois tout mis en place, il faut découper la pièce (le bouton en bas à droite).

![prusa_2](images/module_3/prusa_2.PNG)

Une fois découpée, il faut s'assurer que toutes les proportions, les paramètres et les différentes couches sont comme on le souhaite. Ensuite, il faut exporter le G-code et mettre ce fichier dans l'imprimante 3D pour qu'elle imprime l'objet en question.

### Dans la vraie vie

Dans la vraie vie maintenant, il faut **nettoyer le plateau** pour qu'il n'y ait pas d'imperfections ou d'endroits gras qui puissent modifier l'adhérence de la pièce (et fassent rater l'impression). Il faut aussi faire attention à ce qu'il n'y ait rien en dessous du plateau, de sorte qu'il soit le plus horizontal possible.


### Pièces de test

Imprimer un objet peut rapidement prendre **plusieurs heures**. 
Pour éviter d'attendre le temps d'impression à chaque fois et tester à *tatillon*. Il est bon de faire des pièces test et des négatifs. 

Nous avons utilisé différents *patrons* que nous savons proches de la taille réelle d'un Lego.
En testant ces petits modules sur de vraies briques, nous gagnons un temps énorme et nous pouvons noter nos résultats pour la différence entre la **valeur numérique** et la **réalité**.

L'utilisation de pièces tests comme des "*studs*" (des petits cylindres) pour tester la taille des trous de ma pièce et de Lego pour tester la distance entre les trous m'a permis de diminuer énormément le temps de réalisation de la pièce fonctionnelle. 
L'utilisation de paramètres dans le fichier **.scad** aussi, pour modifier la pièce rapidement.

```
// space between the two holes (cylinders)
SPACE_BETWEEN=8;

// diameter of holes (cylinders)
DIAMETER_HOLE=4.85;

//error between real life and openscad
ERROR=0.22;
```

Ceci est un **exemple** de pièces test que nous avons effectué. Nous avons aussi fait la version négative (les trous) et une version pour la flexibilité (en variant l'épaisseur).

![stud](images/module_3/test_stud.PNG)

*studs pour tester la taille des trous*

Il existe des pièces **test** sur [Thingiverse](https://www.thingiverse.com/) pour tester les capacités, la précision et la comparaison logiciel/réalité comme ceux-ci :

![tests](images/module_3/tests.PNG)

## Mécanisme 

Pour le **mécanisme de groupe**, je me suis mis avec mon binôme de soutiens [**Thibault Leonard**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault/) et avec [**Victor De Pillecyn**](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn).

Voici le rendu final de **ma pièce** : 

![](images/module_3/piecelouis.jpg)

Voici les autres pièces imprimées (pièces de **Thibault Leonard** et de **Victor De Pillecyn**) :

![Pièce de thibault](images/module_3/piecethib.jpg)

*Pièce de Thibault* 

![](images/module_3/piecevictor.jpg)

*Pièce de Victor*

Nous avons assemblé nos pièces pour créer un porte un porte-clef qui rebondit :

![](images/module_3/porte_clef.jpg)

*Mécanisme final*

## Conclusion

Une imprimante 3D est une technologie très intéressante pour passer d'un objet 3D à un objet physique. La conversion en physique impose beaucoup de contraintes (le temps par exemple) et beaucoup d'imprévus.

J'ai rencontré quelques **difficultés** lors de la réalisation de ce module. En effet, il a fallu plusieurs tests (4-5) juste pour l'impression de la pièce et plusieurs pièces test (les **studs**, les briques **Lego**) pour régler les paramètres de celle-ci.
Ma pièce étant assez petite, elle s'imprimait en quelques minutes, cela a donc aidé à faire les tests d'impressions plus vite.

Les tests m'ont permis de mettre en lien ma pièce et celles de mes deux collègues **Thibault Leonard** et **Victor De Pillecyn**. Le tout en ne faisant que 5 impressions au lieu de quelques dizaines si j'y étais allé à l'essai/erreur.

Je suis assez satisfait du rendu final de mon connecteur flexible, car il est assez **malléable**. Cependant, il est un peu trop **fragile** au niveau des connexions, mais une connexion trop épaisse empêcherait le connecteur d'être flexible.

Le **rendu final** devrait être le suivant. Sinon, il faut ajuster les paramètres dans PrusaSlicer.
![rendu vraie vie](images/module_3/IRL.jpg)


## Check-list

- [X] In group, test the design rules, comment on the choice of materials and good practices for your 3D printer(s)
  **regarding the kit parts you are going to make.**
- [X] In group, test and set the parameters of your flexible part so they can be combined with other classmate parts.
- [X] Individually, 3D print your flexible construction kit you made.
- [X] In group, make a compliant mechanism out of it with several hinges that do something.
