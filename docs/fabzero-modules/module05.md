# 5. Dynamique de groupe et projet final

Cette semaine, nous avons choisi des **problématiques** qui nous tenaient à cœur et créé des groupes en fonction de ces dernières. 
Nous avons dû mettre nos idées en commun et apprendre à réfléchir **en groupe**.

Nous avons ensuite du choisir une **thématique** et une **problématique**. 
Nous avons ensuite du créer un projet en passant par la phase d'**idées**, de **maquettage**, de **prototypage** et de **finalisation**.

## Groupe 5 - Mushroom X 

[Mushroom X - *LockBin* (groupe 5)](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-05/)

| Nom                                                                                                                            | Objet choisi                                                                          | Signification                                                                     |
|--------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| [Donovan CROUSSE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/donovan.crousse/)          | Bananes                                                                               | L'accumulation des déchets                                                        |
| [Christop ORY](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/christophe.ory/)              | Un goblet de café avec du plastique recyclé (sensé être meilleur pour l'environnement | La gestion des déchets et le green whashing                                       |
| [Alexandre HALLEMANS](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/) | Des baguettes (pour manger) en bois                                                   | L'utilisation des matières premières et la pénurie qui tourne autour              |
| [Kawtar ZAOUIA](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Kawtar.zaouia/)              |                                                                                       | Kawtar est arrivée plus tard dans le groupe et n'avait donc pas d'objet avec elle |  
| [Louis DEVROYE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.devroye/)              | Pot de produit cosmétique/bien être *[Cerave](https://www.cerave.be/fr-be)*           | Bien être animal (test sur les animaux) et rejet des déchets chimiques dans l'eau | 

## Objet choisi et signification

J'ai choisi un objet de **soins pour la peau** dans le but de représenter l'**industrie pharmaceutique** et **cosmétique**. 
La problématique qui va avec est le **bien-être animal** pour les *tests effectués* et la **pollution des eaux** à cause des *substances toxiques* déversées par cette industrie.

![Cosmétique](images/module_5/cosmetique.jpg)

*Cosmétique Cerave*

## Groupe et approche

Sur base de cet objet, j'ai rejoint des personnes ayant eu le **gaspillage** comme thématique car je n'avais personne sur la défense animale (que je préférais un peu au gaspillage). 
Nous nous sommes présentés nous et notre objet (et la problématique qui va avec). Après cela, nous avons mis nos idées en commun et vu sur quel terrain nous pourrions être d'accord. Nous nous sommes accordés sur "**le Recyclage**", une thématique vaste, mais importante pour le futur.

Pour départager nos idées nous avons chacun mis un point important et tout le monde a voté pour chaque point en fonction de l'importance qu'il y accordait (cf. **jugement majoritaire**).
![importance](images/module_5/panneau.jpg)

*Jugement majoritaire thématique*

Pour ce projet, je n'ai pas trouvé de groupe ayant ma thématique/problématique principale (le bien être animal). 
J'ai donc fait des compromis pour essayer de coller avec le groupe. Je ne voulais pas de tension alors j'ai suivis.

## Thématique finale

La thématique que nous avions retenue est "**le Recyclage**". 
Nous avions fait un tour des idées sur lesquelles nous pourrions nous tourner, cela se jouait entre : 

- l'eau

- les matières premières

- un matériau organique (comme des champignons) pour absorber des matières toxiques.

- le plastique

## Aides des autres groupes

Une personne de chaque groupe est partie en "**porte-parole**" pour voir avec les autres si notre thématique leur parlait et s’ils avaient des problématiques qu'ils pensaient être importantes.

![feuille](images/module_5/feuille.jpg)

*Idées porte-parole*


## Arbre à problèmes-solutions

Un **arbre à problèmes-solutions** est une méthode utilisée sur un sujet en particulier pour résoudre les problèmes liés à celui-ci.
Il s'agit en réalité de deux arbres, un pour les **problèmes** et l'autre pour les **solutions**.
Les deux sont structuré avec :

- Le **sujet** au milieu du tronc.

- Un côté (feuille ou racines) pour les **causes**.

- L'autre (racines ou feuilles) des **conséquences**.

La méthode consiste en : 

- créer un arbre à problèmes.

- Une fois cet arbre fini, l'analyser.

- Créer un second arbre des solutions cette fois-ci. En tenant compte du premier arbre.

### Exemple - gaspillage des composants électroniques 

Voici un exemple d'un arbre à problèmes et d'un arbre à solutions pour le thème du *gaspillage des composants électroniques*

#### Arbre à problèmes

![arbre à problèmes](images/module_5/arbre_prob.png)

*Arbre à problèmes*

#### Arbre à solutions

![arbre à solutions](images/module_5/arbre_sol.png)

*Arbre à solutions*

## Arbre à problèmes du groupe

Nous avons dessiné un arbre à problème (assez brouillon) :

![Arbre à problèmes](images/module_5/arbre_groupe.png)

*Arbre à problèmes du groupe (5)*

## Choix de la solution

Nous avons choisi dès le début de nous tourner vers une poubelle qui flotterait à la surface de l'eau.
Cette poubelle serait positionnée dans un cours d'eau belge comme la Meuse ou le canal de Bruxelles, au niveau d'une écluse. Elle capterait les gros déchets comme des bouteilles, des emballages et possiblement des mégots.
Pour la vider, il faut retirer le sac qu'elle contient et puis le raccrocher sur la poubelle. Elle ne coule pas avec l'eau car il n'y a qu'un filet pour retenir les déchets, l'eau passe donc fluidement à travers.

Un exemple de notre solution pourrait être une "[Seabin](https://www.youtube.com/shorts/SWaFfjf-E8c)" (lien youtube)

Cette solution a apporté beaucoup de questions et de problèmes de mise en pratique et de visualisation.

# Remaniement de dernières minutes

À cause d'un manque de communication au sein de l'équipe, nous avons pris énormément de retard. 
Lors de notre entretien avec Denis, nous n'avions qu'une maquette en carton, là où nous aurions dû avoir un prototype qui fonctionne ou du moins voir les défauts pour les améliorer.

Nous avons donc essayé de prendre les choses en mains mais à cause du manque de temps, du stress que ça engendre et des examens, nous avons pris les mauvaises décisions.
Cela nous a mené à une "usine à vapeur". Nous voulions résoudre 500 problèmes mais sans avoir rien de concret.

Par exemple, nous voulions gérer : la **gestion d'électricité**, des **déchets**, l'ajout du **système** dans une écluse (qui n'est que très difficilement possible dans la vraie vie et peu efficace). 
Certains points étaient même **contre productifs** où nous récoltions l'énergie de l'eau lors du vidage de l'écluse et nous l'utilisions pour créer un courant au-dessus avec une pompe. 

Nous avons donc abandonné la **gestion d'électricité** qui est déjà très bien faite dans la vraie vie, et nous nous sommes concentrés sur simplifier la conception de la poubelle pour qu'elle soit plus accessible.

Nous avons utilisé l'**impression 3D** pour créer le prototype de la poubelle.

# Dynamique de groupe

## Art de décider

Décider des choses à plusieurs (ou non) peut être une tâche complexe. Pour aider un maximum, il existe des **méthodes de décision** :

- consensus

- vote

- jugement majoritaire

- choix sans objection

- etc.

Nous avons utilisé le jugement majoritaire pour la grande majorité des décisions car cela semblait être le meilleur compromis. 

Il existe 3 grandes branche de la décision :

- **Décider seul(e)** : On avance vite mais pas forcément bien, c'est bien pour les petites tâches que l'on doit faire seul ou à 2.

- **Décider en groupe** : Pour les grosses décisions. On avance parfois trop lentement, c'est là qu'interviennent les **méthodes de décision**.

- **Ne pas décider** : Ne pas donner d'avis est aussi donner son avis, c'est qu'on a soit pas d'objection envers la décision soit qu'on n'a pas assez d'éléments pour émettre un avis.

## Rôles dans les réunions

Un projet de groupe implique un nombre assez conséquent de réunions. Pour que celles-ci restent le plus productives possible, nous assignons un rôle à chacun :

- **Secrétaire** : Prend des notes sur ce qu'il se passe.

- **Gestionnaire du temps** : Gère le temps accordé à chaque sujet traité et met un terme à la discussion si elle prend trop de temps par rapport à l'importance du sujet.

- **Gestionnaire de la parole** : Gère les temps de paroles de chacun pour que tous les avis soient entendus (même si la personne ne veut pas parler, c'est qu'elle est d'accord avec le sens de la discussion).

- **Animateur** : Différent du **gestionnaire de la parole**, il aborde les différents sujets et commence l'ordre du jour avec le *température check* ou d'autres introductions de réunion.

## Température Check
La **température check** est une **méthode** pour voir si les personnes concernées par la réunion sont enthousiastes ou non (pas spécialement avec la réunion, mais simplement dans leur journée).

Cette méthode consiste à tendre les bras et à les mettre plus ou moins hauts en fonction de l'énergie que l'on a. 
Les bras au ciel = beaucoup d'énergie/enthousiasme, les bras vers le sol = peu d'énergie/enthousiasme.

Les méthodes d'introduction de réunion **peuvent** être un bon moyen de les commencer. Nous n'avons, cependant, que rarement fait un température check (en tout cas pas aussi précis que comme décris en haut).

# Conclusion 

Dans ce module, j'ai appris à **gérer un projet non informatique en groupe**. 
Nos connaissances et nos chemins étaient beaucoup plus **différents** que pour les projets que j'ai pu avoir jusqu'à aujourd'hui.

J'ai pu comprendre les problématiques de la vraie vie comme la **communication**, la **gestion d'agenda**, la gestion de **deadlines** et surtout la différence de **points de vue**. 
Les projets sont souvent trop petits pour laisser les points de vue rentrer en compte.