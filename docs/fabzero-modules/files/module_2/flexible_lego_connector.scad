/*

    DESCRIPTION  : Projet Fablab (PHYS-F513 Assignement 2), compliant mechanisms. Flexible lego connector.
    
    AUTHOR : Louis Devroye (louis.devroye@ulb.be)
    
    DERIVED FROM :  FlexLinks: Cantilever Beam Adjust (LEGO Compatible) (https://www.thingiverse.com/thing:3016939)
    
    Date : 23/02/23
    
    LICENSE : Creative Commons, Attribution-ShareAlike 4.0 International [CC BY-SA 4.0] 
    https://creativecommons.org/licenses/by-sa/4.0/).

*/


//////// global variables 

// space between the two holes (cylinders)
SPACE_BETWEEN=8;

// diameter of holes (cylinders)
DIAMETER_HOLE=4.85;

//error between real life and openscad
ERROR=0.22;

// scale of the middle cubes depending on the height of the piece
PROPORTION_MIDDLE_CUBE=1;

// difference to get rid of the artifacts in differences
EPSILON=1;

// scale
SCALE=1;

// main variables
LENGTH=37;
WIDTH=LENGTH/4.5;
HEIGHT=1.8;

flexible_connector(LENGTH*SCALE, WIDTH*SCALE, HEIGHT*SCALE);

module base(length, width, height, color="yellow") {
    
    // without the hull we must use a cube inbetween the two cylinders
    // cube([length, width, height], center=true);
    color(color, 1.0) {
        r_cyl = width/2;
        hull(){
            for (i=[-1:1]) {
                if (i!=0) {
                    translate([i*length/2,0,0]) {
                        cylinder(height, r=r_cyl, center=true, $fn=50);
                    }
                }
            }
        }
    }
}

module rounded_cube(length, width, height, color="red") {
    
    //diam = length < width ? length/2 : width/2;
    color(color, 1.0) {
        minkowski(){
            cube([length, width, height], center=true);
            sphere(d=length/2, $fn=50);
        }
    }
}

module negativ_cubes(length, width, height){
    
    //middle
    translate_x = length/2;
    translate_z = height*PROPORTION_MIDDLE_CUBE;
    translate([0,0,translate_z*2.2]) {
        rounded_cube(length, width, height/2);
    }
        
    //left
    translate([translate_x,0,translate_z/2]) {
        rounded_cube(length/3, width, height,"blue");
    }
    
    //right
    translate([-translate_x,0,translate_z/2]) {
        rounded_cube(length/3, width, height,"green");
    }
}

module cylinder_holes(depth, radius, space_between) {
    
    cylinder(depth, r=radius, center=true, $fn=100);
    
    
    translate([space_between,0,0]) {
        cylinder(depth, r=radius, center=true, $fn=100);
    }
}


module flexible_connector(length, width, height, color="yellow") {
    
    color(color, 1.0) {
        difference() {
            
            base(length, width, height);
            
            ////holes
            neg_l = length/4;
            neg_h=height/3*2;
            negativ_cubes(neg_l, width, neg_h);

            //cylinder
            
            radius = (ERROR+DIAMETER_HOLE)/2*SCALE;
            space_between = SPACE_BETWEEN*SCALE;
            translate([-length/2,0,0]) {
                cylinder_holes(height+EPSILON, radius, space_between);
            }
            translate([(length/2)-space_between,0,0]) {
                cylinder_holes(height+EPSILON, radius, space_between);
            }
        }
        
    }    
    
}
