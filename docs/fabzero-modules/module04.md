# 4. Outil Fab Lab sélectionné : Microcontrôleur

Cette semaine, j'ai travaillé sur un microcontrôleur en vue d'apprendre à ajouter des modules en toute sécurité (par rapport aux voltages, courts circuits, etc.). Nous avons travaillé sur le [YD-RP2040]() et tout un kit fournis avec. L'excercice était d'installer un IDE ([Thonny](https://thonny.org/) ou [Arduino](https://www.arduino.cc/en/software)), par après, lier cet environnement de développement (**IDE**) au microcontrôleur et enfin, programmer sur ce micro controleur et lier des modules externes (de [DTH20]() ou [SEN0364](https://wiki.dfrobot.com/Gravity_AS7341_Visible_Light_Sensor_SKU_SEN0364) par exemple) dessus pour communiquer avec.

## Liens utiles

| Nom                                                                                                                                                                                                 | Description                                                                                                                                                                                                          |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [**IDE Adrunio**](https://www.arduino.cc/en/software)                                                                                                                                               | Arduino est environnement de développement (IDE) et un langage de programmation dérivé du [C](https://fr.wikipedia.org/wiki/C_(langage)), il permet de créer des programmes informatiques pour des microcontrôleurs. |
| [**Thonny IDE**](https://thonny.org/)                                                                                                                                                               | Thonny est un environnement de développement qui supporte python (et microPython)                                                                                                                                    |
| **SEN0364** ([wiki](https://wiki.dfrobot.com/Gravity_AS7341_Visible_Light_Sensor_SKU_SEN0364), [github](https://github.com/DFRobot/DFRobot_AS7341))                                                 | outil fablab pour détecter les différentes fréquences de couleurs                                                                                                                                                    |
| [**dht20**](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf)                                                                                                                                    | outil du kit permettant de capter la température et l'humidité ambiante                                                                                                                                              |
| [**YD-RP2040**](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/)                                                                                                                    | Microcontrôleur programmable                                                                                                                                                                                         |
| [**MicroPython**](https://micropython.org/)                                                                                                                                                         | version embarquée de python permettant de programmer sur un microcontroleur                                                                                                                                          |
| [**FabZero experiment - Electronic prototyping**](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/selected-fab-tool/electronic_prototyping/) | Site de l'exercice du fablab                                                                                                                                                                                         |
| [**Guide pour programmer sur le RP2040**](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/)                                                                                          |                                                                                                                                                                                                                      |

**Tout le code et les fichiers qui ont été nécessaires sont dans le dossier files/module_4**

## Introduction YD-RP2040

Le **YD-RP2040** est un microcontrôleur, les microcontrôleurs sont des "petits" (quelques centimètres) circuits électriques programmables.
Ils sont souvent utilisés en combinaisons avec des capteurs et petits moteurs, car ils sont simples et peu cher.  

La communication entre le microcontrôleur et des modules externes se fait par : 
- les **pins** *entrants* et *sortants* (trou et piques)
- le **port USB**

La connexion par **USB** se fait assez instinctivement, il suffit de brancher un cable USB vers un ordinateur ayant microPython, Arduino ou un IDE pour capter ce microcontroleur.

Par **pin**, les choses se compliquent, il faut savoir quel pin est disponible et quel type de pin c'est. Il y a ceux d'informations et d'alimentation. 
Pour **l'alimentation**, on peut suivre l'exemple d'une batterie de voiture avec un cable rouge vers rouge et un cable noir vers noir. Il faut faire attention au voltage que le module peut support (ou dont il a besoin).
Pour **l'information**, il y a différents types. Ceux utilisés ici sont **SDA** (Serial Data Line) : ligne de données bidirectionnelle (envoi bidirectionel de données entre le microcontrôleur et le module) et **SCL** (Serial Clock Line) : ligne d'horloge de synchronisation bidirectionnelle (synchroniser le module avec le microcontrôleur).

Pour connaitre les pins, les paramètres d'alimentation et d'autres informations utiles. Il faut se fier aux **Data sheet** du module externe et du microcontrôleur.
(Je montre par la suite comment configurer les pins SDA et SCL dans le code).

## Le boot load de UF2

Un microcontrôleur est programmé en injectant du code dans sa mémoire à l'aide d'un programmeur dédié, qui est un matériel spécialisé. 
Cependant, un bootloader est un petit programme pré-installé dans la mémoire du microcontrôleur, qui permet à l'utilisateur de communiquer avec la puce pour injecter un nouveau programme en utilisant des interfaces telles que le port série ou l'USB.

**UF2** : 

- Bootloader intégré sur la puce (pas besoin de "programmeur").

- Communication via **USB**

- Développer par **Microsoft** : [lien](https://makecode.com/blog/one-chip-to-flash-them-all)

Pour entrer en **boot-mode** (pour programmer la puce) le boutton "boot" doit rester appuyer lors du démarrage :

- Appuyez sur le bouton de démarrage tout en branchant le module sur le port USB.

- **OU** : appuyer sur le bouton de démarrage, appuyer puis relâcher le bouton de réinitialisation, de sorte que le bouton de démarrage reste enfoncé pendant que la puce redémarre.

- Le module ressemble maintenant à une "mémoire de masse" (**clé usb**) pour le *système d'exploitation* et vous pouvez copier votre code pour programmer la puce (ou le faire via votre interface de programmation). (ou le faire via votre interface de programmation)

## Langages & IDE (microPython, Arduino)

J'ai utilisé **microPython** et **Adruino**, respectivements sous **Thonny** et l'**IDE Arduino**.

### Thonny 

Pour configurer Thonny avec microPython :

- Télécharger [Thonny](https://thonny.org/). 

- *pip install thonny* dans un terminal de commande.

- Aller dans **Tools**->**options**->**interpreter : rp2040**

- relancer rp2040 en *boot mode* (lancer en restant appuyé sur le bouton boot)

- cliquer sur *install or update MicroPython*

- ok => sélectionner pico comme *target* (pico & xiao-rp2040 agissent de la même façon).

### Arduino

Pour configurer Arduino sur l'IDE du même nom :

- Télécharger l'[IDE Arduino](https://www.arduino.cc/en/software) et le [board RP2040](https://github.com/earlephilhower/arduino-pico#installing-via-arduino-boards-manager).

- Ajouter le [github Arduino](https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json) au "Board Manager" (dans "Preferences").

- Aller dans **Tools**->**Board**->**Board manager** et installer le package "*pico*" (*Raspberry Pi Pico/RP2040*). 

## Capteur de température et humidité DHT20

### Description

Le capteur **DHT20** est un capteur de température (en **°C**) et d'humidité (en **%**).

**Les fichiers microPython utilisés pour les illustrations sont dans le fichier /files/module_4**

### Branchements

Pour le brancher, il faut se référer à ce schéma ci-dessous pour le SDA et SCL. On peut changer l'entrée SCL et SDA dans Thonny (en python) avec :

```
i2c0_sda = Pin(pin1)
i2c0_scl = Pin(pin2)
```

Pour lancer la connection avec les pins, il faut utiliser cette fonction :
```
i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)
```

Comment placer les cables pour brancher le dht20 :
![](images/module_4/placement_cables.png)

*Schéma du placement des cables - DHT20*

Fiche technique pour brancher le dht20 :
![](images/module_4/DHT20.png)

*Fiche technique - DHT20*


### Dans la vraie vie 


Le kit devrait être brancher comme cela :

![](images/module_4/connexion_irl_DHT20.jpg)

*Connections dans la vraie vie - DHT20*

Lors de son fonctionnement, des données devraient apparaitre sur l'écran.
![](images/module_4/fonctionnement_donnees.jpg)
*Déroulement des données lors du fonctionnement - DHT20*

## Capteur de lumière : SEN0364

### Description

Le capteur **SEN0364** est un capteur de *lumière* et plus précisément, de **couleurs**.
Son fonctionnement plus appliqué est expliqué ci-dessous avec les fiches techniques.

**Les fichiers Arduino utilisés pour les illustrations sont dans le fichier /files/module_4**

### Branchements
On peut changer l'entrée **SCL** et **SDA** dans arduino avec :
```
Wire.setSDA(pin1)
Wire.setSCL(pin2)
```

Pour lancer la connection avec les pins choisi, il faut mettre :
```
Wire.begin()
```

### Fiches techniques du capteur

Spectre des couleurs qui sont captées :
![](images/module_4/couleurs_lightsensor.png)
*Spectre des couleurs captées - SEN0364*

Fiche électrique du capteur :
![](images/module_4/fiche_elec_lightsensor.png)
*Fiche électrique - SEN0364*

Pour le brancher, il faut se référer à ce schéma du **SDA** et du **SCL** :
![](images/module_4/SEN0364_cablage.png)
*Cablage - SEN0364*

### Exemples en fonctionnement

Lorsque le capteur reconnait du bleu :
![](images/module_4/bleu.png)
*Reconnaissance de bleu - SEN0364*

Lorsque le capteur reconnait du vert :
![](images/module_4/vert.png)
*Reconnaissance de vert - SEN0364*


### Dans la vraie vie 

Le module ressemble à cela :
![](images/module_4/light_sensor_irl.jpg)
*Image - SEN0364*

Lorsqu'il est branché, le tout ressemble à cela :
![](images/module_4/SEN0364_IRL.jpg)
*Fonctionnement - SEN0364*

## Conclusion

Lors de ce **module**, j'ai dû apprendre à utiliser un **microcontrôleur** et à le **programmer** par moi-même.

L'utilisation de ce genre d'outil est assez **complexe** car elle nécessite des connaissances **électriques** et **informatiques**.
Même avec ces connaissances, il faut toujours vérifier les **data sheet** des produits pour bien les configurer et pour ne pas casser certains composants.

J'ai donc appris à bien m'informer sur un outil avant de commencer à l'utiliser (par peur de faire des erreurs dans le cablage qui seraient fatales pour le microcontrôleur ou le module).

## Check-list

- [X] Describe how an IDE works on the microcontroller.
- [X] Explain and show how to insert some code into the microcontroller.
- [X] Show a working example of color recognition by the microcontroller.
