
Bienvenue sur mon site créé dans le cadre du cours 
[How To Make (almost) Any Experiments Using Digital Fabrication](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

## Introduction

Salut moi c'est **Louis Devroye** j'ai 20 ans et je suis en 3ème année en sciences informatiques à l'ULB !

![(toujours à l'aise pour les photos)](images/photo.jpg)


#### Mes passions

J'ai toujours été curieux du monde qui m'entourait et j'ai très vite été interessé par l'informatique notamment à travers les jeux vidéos. Mon grand rève de jeunesse était de faire des mod sur le jeu cubique connu de tous [**minecraft**](https://www.minecraft.net/fr-)


J'ai fait 6 ans de **badminton** dans ma jeunesse (j'ai sans doute tout perdu). 
Aujourd'hui je me tourne plus vers la [callisthénie](https://fr.wikipedia.org/wiki/Callisth%C3%A9nie) et la natation.

#### Mes objectifs

Pour ce cours, j'aimerais apprendre de nouvelles techniques et découvrir de nouvelles choses en liens avec l'informatique ou non.

En général, j'aimerais créer des outils utiles et/ou des innovations utiles au monde de demain. 

## Projets récents

Dans le cadres de certains cours J'ai réalisé différents projets tels que :

* une [blockchain](https://fr.wikipedia.org/wiki/Blockchain) pour une institution d'échec avec un système de [Classement Elo](https://fr.wikipedia.org/wiki/Classement_Elo). Le tout désentralisé et ayant une app android (en [P2P](https://fr.wikipedia.org/wiki/Pair-%C3%A0-pair)).
* un jeu de dame modifié [jeu des amazones](https://en.wikipedia.org/wiki/Game_of_the_Amazons)
* un balancier électronique
* une copie (simpliste) de [candy crush](https://fr.wikipedia.org/wiki/Candy_Crush_Saga).
* une application [Quoridor](https://fr.wikipedia.org/wiki/Quoridor) avec une interface graphique et un sytème client/serveur pour s'envoyer des messages et se connecter à une base de donnée utilisateurs.

J'ai aussi réalisé des projets personnels tels que :

* [Game of Life](https://fr.wikipedia.org/wiki/Jeu_de_la_vie), un automate imaginé par John Conway
* un jeu basique inspiré du célèbre [Pong](https://fr.wikipedia.org/wiki/Pong), avec des animations des sons, un score et des sauvgardes
* d'autres petits projet de jeu et un gros projet en 

Certains de mes projets personnels peuvent être trouvés sur [itch.io](https://shachoca.itch.io/)

Voilà pour ma présentation ! 


